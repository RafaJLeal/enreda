Componentes utilizados:

 - python 3.7
 - django 2.2
   - djangorestframework
   - django-cors-headers
 - virtualenv
 - npm: - vue.js (CLI) - axios - vue-form-generator - vue-bootstrap - sweetalert

Despliegue:

 - en ../Enreda_note
   - python manage.py migrate
   - python manage.py runserver
 - en otro terminal ../Enreda_note/frontend
   - npm run dev
 - acceder a un navegador web y poner http://localhost:8080/
